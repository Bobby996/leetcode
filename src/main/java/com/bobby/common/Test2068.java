package com.bobby.common;

/**
 * Created by wenjunpei
 * in 2021/12/17 16:40.
 */
public class Test2068 {
    public static void main(String[] args) {

    }
    public boolean checkAlmostEquivalent(String word1, String word2) {
        int[] a1 = new int[26];
        int[] a2 = new int[26];
        for (int i = 0; i < word1.length(); i++) {
            char c1 = word1.charAt(i);
            a1[c1 - 'a'] += 1;
        }
        for (int i = 0; i < word2.length(); i++) {
            char c2 = word2.charAt(i);
            a2[c2 - 'a'] += 1;
        }
        for (int i = 0; i < a1.length; i++) {
            if(Math.abs(a1[i] - a2[i]) > 3) {
                return false;
            }
        }
        return true;
    }
}
