package com.bobby.offer;

/**
 * Created by wenjunpei
 * in 2022/1/10 9:06.
 */
public class Test070 {
    public static void main(String[] args) {

    }
    public int singleNonDuplicate(int[] nums) {
        int target = 0;
        for (int i = 0; i < nums.length; i++) {
            target ^= nums[i];
        }
        return target;
    }
}
