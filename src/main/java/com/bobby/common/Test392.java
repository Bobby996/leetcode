package com.bobby.common;

/**
 * Created by wenjunpei
 * in 2021/12/3 10:53.
 */
public class Test392 {
    public static void main(String[] args) {
        System.out.println(isSubsequence("aaaaaa","bbaaaa"));
    }
    public static boolean isSubsequence(String s, String t) {
        int index = -1;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int temp = t.indexOf(c,index + 1);
            if(temp == -1) {
                return false;
            }else {
                if(temp <= index) {
                    return false;
                }else {
                    index = temp;
                }
            }
        }
        return true;
    }
}
