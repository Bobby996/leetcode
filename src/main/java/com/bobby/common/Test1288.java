package com.bobby.common;

/**
 * Created by wenjunpei
 * in 2021/11/8 14:25.
 */
public class Test1288 {
    public static void main(String[] args) {
        System.out.println(removeCoveredIntervals(new int[][]{{1,2},{1,4},{3,4}}));
    }
    public static int removeCoveredIntervals(int[][] intervals) {
        int sum = intervals.length ;
        for (int i = 0; i < intervals.length - 1; i++) {
            for (int j = i + 1; j < intervals.length; j++) {
                if(intervals[i][0] != -1 && intervals[i][1] != -1 && intervals[j][0] != -1 && intervals[j][1] != -1) {
                    if(intervals[j][0] <= intervals[i][0] && intervals[i][1] <= intervals[j][1]) {
                        sum--;
                        intervals[i][0] = -1;
                        intervals[i][1] = -1;
                    }else if(intervals[i][0] <= intervals[j][0] && intervals[j][1] <= intervals[i][1]) {
                        sum--;
                        intervals[j][0] = -1;
                        intervals[j][1] = -1;
                    }
                }
            }
        }
        return sum;
    }
}
