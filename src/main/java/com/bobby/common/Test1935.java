package com.bobby.common;

/**
 * Created by wenjunpei
 * in 2021/10/21 18:38.
 */
public class Test1935 {
    public static void main(String[] args) {
        Test1935 test1935 = new Test1935();
        System.out.println(test1935.canBeTypedWords("hello world", "ad"));
    }
    public int canBeTypedWords(String text, String brokenLetters) {
        String[] array = text.trim().split(" ");
        int count = 0;
        boolean flag = true;
        for (String s : array) {
            flag = true;
            for (int i = 0; i < brokenLetters.length(); i++) {
                if(s.contains(String.valueOf(brokenLetters.charAt(i)))) {
                    flag = false;
                    break;
                }
            }
            if(flag) {
                count++;
            }
        }
        return count;
    }
}
