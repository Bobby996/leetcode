package com.bobby.common;

import java.util.Arrays;

/**
 * Created by wenjunpei
 * in 2021/11/16 16:49.
 */
public class Test1913 {
    public static void main(String[] args) {

    }
    public int maxProductDifference(int[] nums) {
        Arrays.sort(nums);
        int length = nums.length;
        return nums[length - 1] * nums[length - 2] - nums[0] * nums[1];
    }
}
