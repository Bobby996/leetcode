package com.bobby.common;

/**
 * Created by wenjunpei
 * in 2021/11/16 17:13.
 */
public class Test2027 {
    public static void main(String[] args) {

    }
    public int minimumMoves(String s) {
        int count = 0 ;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if(c == 'X') {
                count++;
                i += 2;
            }
        }
        return count;
    }
}
