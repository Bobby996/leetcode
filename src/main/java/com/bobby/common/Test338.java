package com.bobby.common;

/**
 * Created by wenjunpei
 * in 2021/11/9 9:25.
 */
public class Test338 {
    public static void main(String[] args) {

    }
    public static int[] countBits(int n) {
        int[] target = new int[ n + 1];
        for (int i = 0; i <= n; i++) {
            int count = 0;
            int temp = i;
            while(temp != 0) {
                count += temp & 1;
                temp = temp >> 1;
            }
            target[i] = count;
        }
        return target;
    }
}
