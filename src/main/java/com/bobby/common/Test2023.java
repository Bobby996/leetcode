package com.bobby.common;

/**
 * Created by wenjunpei
 * in 2022/1/6 9:39.
 */
public class Test2023 {
    public static void main(String[] args) {

    }
    public int numOfPairs(String[] nums, String target) {
        int count = 0 ;
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length; j++) {
                if(i != j) {
                    if((nums[i] + nums[j]).equals(target)) {
                        count++;
                    }
                }
            }
        }
        return count;
    }
}
