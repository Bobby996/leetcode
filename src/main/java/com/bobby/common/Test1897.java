package com.bobby.common;

/**
 * Created by wenjunpei
 * in 2021/12/3 9:53.
 */
public class Test1897 {
    public static void main(String[] args) {

    }
    public static boolean makeEqual(String[] words) {
        int[] array = new int[26];
        for (String word : words) {
            for (int i = 0; i < word.length(); i++) {
                char c = word.charAt(i);
                array[c - 'a'] += 1;
            }
        }
        int length = words.length;
        for (int i : array) {
            if(i % length != 0) {
                return false;
            }
        }
        return true;
    }
}
