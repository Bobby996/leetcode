package com.bobby.common;

import java.util.Arrays;

/**
 * Created by wenjunpei
 * in 2021/10/21 17:29.
 */
public class Test66 {
    public static void main(String[] args) {
        Test66 test66 = new Test66();
        System.out.println(Arrays.toString(test66.plusOne(new int[]{9, 9, 9, 9})));
    }
    public int[] plusOne(int[] digits) {
        int jinwei = 1;
        for (int i = digits.length - 1; i >= 0; i--) {
            int temp = digits[i] + jinwei;
            digits[i] = temp % 10;
            jinwei = temp / 10;
        }
        if(jinwei != 0) {
            int[] target = new int[digits.length + 1];
            for (int j = target.length - 1; j > 0; j--) {
                target[j] = digits[j - 1];
            }
            target[0] = jinwei;
            return target;
        }else {
            return digits;
        }
    }
}
