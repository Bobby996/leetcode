package com.bobby.common;

import java.util.HashSet;
import java.util.Iterator;

/**
 * Created by wenjunpei
 * in 2021/11/8 11:35.
 */
public class Test1390 {
    public static void main(String[] args) {
        System.out.println(sumFourDivisors(new int[]{21,4,7}));
    }

    public static int sumFourDivisors(int[] nums) {
        Integer sum = 0;
        for (int num : nums) {
            HashSet<Integer> hashSet = new HashSet<>();
            hashSet.add(1);
            hashSet.add(num);
            for (int i = 2; i <= num - 1; i++) {
                if(hashSet.size() > 4) {
                    break;
                }
                if(num % i == 0) {
                    hashSet.add(i);
                    hashSet.add(num / i);
                }
            }
            if(hashSet.size() == 4) {
                Iterator<Integer> iterator = hashSet.iterator();
                while(iterator.hasNext()) {
                    Integer next = iterator.next();
                    sum += next;
                }
            }
        }
        return sum;
    }
}
