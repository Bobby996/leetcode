package com.bobby.common;

/**
 * Created by wenjunpei
 * in 2021/12/3 16:59.
 */
public class Test1848 {
    public static void main(String[] args) {

    }
    public int getMinDistance(int[] nums, int target, int start) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < nums.length; i++) {
            if(nums[i] == target) {
                int temp = Math.abs(i - start);
                if(temp < min) {
                    min = temp;
                }
            }
        }
        return min;
    }
}
