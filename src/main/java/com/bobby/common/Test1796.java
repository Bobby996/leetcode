package com.bobby.common;

/**
 * Created by wenjunpei
 * in 2021/12/3 10:24.
 */
public class Test1796 {
    public static void main(String[] args) {
        System.out.println(secondHighest("1a0"));
    }
    public static int secondHighest(String s) {
        int[] array = new int[10];
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if(c >= '0' && c <= '9') {
                array[c - '0'] += 1;
            }
        }
        Integer number = 0;
        Integer count = 0;
        for (int i = array.length - 1; i >= 0; i--) {
            if(count == 2) {
                break;
            }
            if(array[i] != 0) {
                number = i;
                count++;
            }
        }
        if(count != 2) {
            return -1;
        }else {
            return number;
        }
    }
}
