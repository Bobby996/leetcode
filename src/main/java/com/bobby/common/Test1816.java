package com.bobby.common;

/**
 * Created by wenjunpei
 * in 2021/11/8 15:20.
 */
public class Test1816 {
    public static void main(String[] args) {
        System.out.println(truncateSentence("chopper is not a tanuki",5));
    }
    public static String truncateSentence(String s, int k) {
        int index = -1;
        int count = 0 ;
        while(count != k) {
            index = s.indexOf(" ",index + 1);
            if(index != -1) {
                count++;
            }else {
                break;
            }
        }
        if(index == -1) {
            return s;
        }else {
            return s.substring(0,index);
        }
    }
}
