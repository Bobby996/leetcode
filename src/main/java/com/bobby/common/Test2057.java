package com.bobby.common;

/**
 * Created by wenjunpei
 * in 2021/12/13 9:48.
 */
public class Test2057 {
    public static void main(String[] args) {

    }
    public int smallestEqual(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            if(i % 10 == nums[i]) {
                return i;
            }
        }
        return -1;
    }
}
