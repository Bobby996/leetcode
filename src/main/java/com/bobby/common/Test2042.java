package com.bobby.common;

/**
 * Created by wenjunpei
 * in 2021/11/8 15:10.
 */
public class Test2042 {
    public static void main(String[] args) {
        System.out.println(areNumbersAscending("sunset is at 7 51 pm overnight lows will be in the low 50 and 60 s"));
    }
    public static boolean areNumbersAscending(String s) {
        String[] split = s.split(" ");
        int temp  = -1;
        for (int i = 0; i < split.length; i++) {
            try{
                int num = Integer.parseInt(split[i]);
                if(num > temp) {
                    temp = num;
                }else {
                    return false;
                }
            }catch (Exception e) {

            }
        }
        return true;
    }
}
