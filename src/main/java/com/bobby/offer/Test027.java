package com.bobby.offer;

/**
 * Created by wenjunpei
 * in 2021/11/9 9:47.
 */
public class Test027 {
    static class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }
    public static void main(String[] args) {

    }
    public boolean isPalindrome(ListNode head) {
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        while(head != null) {
            sb1.append(head.val);
            sb2.append(head.val);
            head = head.next;
        }
        return sb1.reverse().toString().equals(sb2.toString());
    }
}
