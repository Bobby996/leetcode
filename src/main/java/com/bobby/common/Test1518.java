package com.bobby.common;

/**
 * Created by wenjunpei
 * in 2021/12/17 16:20.
 */
public class Test1518 {
    public static void main(String[] args) {
        System.out.println(numWaterBottles(9,3));
    }
    public static int numWaterBottles(int numBottles, int numExchange) {
        int target = numBottles;
        int kongjiuping = numBottles;
        while(kongjiuping >= numExchange) {
            int xinzeng = kongjiuping / numExchange;
            target += xinzeng;
            kongjiuping -= xinzeng * numExchange ;
            kongjiuping += xinzeng;
        }
        return target;
    }
}
