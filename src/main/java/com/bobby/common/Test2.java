package com.bobby.common;

/**
 * Created by wenjunpei
 * in 2021/10/21 16:12.
 */
public class Test2 {
    static class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }
    public static void main(String[] args) {
        ListNode head1 = new ListNode(2);
        head1.next = new ListNode(4);
        head1.next.next = new ListNode(3);

        ListNode head2 = new ListNode(5);
        head2.next = new ListNode(6);
        head2.next.next = new ListNode(4);

        ListNode head3 = new ListNode(0);
        ListNode head4 = new ListNode(0);

        ListNode head5 = new ListNode(9);
        head5.next = new ListNode(9);
        head5.next.next = new ListNode(9);
        head5.next.next.next = new ListNode(9);
        head5.next.next.next.next = new ListNode(9);
        head5.next.next.next.next.next = new ListNode(9);
        head5.next.next.next.next.next.next = new ListNode(9);

        ListNode head6 = new ListNode(9);
        head6.next = new ListNode(9);
        head6.next.next = new ListNode(9);
        head6.next.next.next = new ListNode(9);
//        ListNode head = addTwoNumbers(head1, head2);
//        ListNode head = addTwoNumbers(head3, head4);
        ListNode head = addTwoNumbers(head5, head6);
        while(head != null) {
            System.out.println(head.val);
            head = head.next;
        }
    }
    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode target = new ListNode();
        ListNode current = target;
        int jinwei = 0;
        while (l1 != null || l2 != null) {
            int tempValue = 0;
            if(l1 != null) {
                tempValue += l1.val;
                l1 = l1.next;
            }
            if(l2 != null) {
                tempValue += l2.val;
                l2 = l2.next;
            }
            tempValue += jinwei;
            ListNode temp = new ListNode(tempValue % 10);
            jinwei = tempValue / 10;
            current.next = temp;
            current = current.next;
        }
        if(jinwei != 0) {
            current.next = new ListNode(jinwei);
        }
        return target.next;
    }
}
