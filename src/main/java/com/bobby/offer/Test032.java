package com.bobby.offer;

/**
 * Created by wenjunpei
 * in 2021/12/14 8:54.
 */
public class Test032 {
    public static void main(String[] args) {

    }
    public boolean isAnagram(String s, String t) {
        if(s.equals(t) || s.length() != t.length()) {
            return false;
        }else {
            int[] a = new int[26];
            int[] b = new int[26];
            for (int i = 0; i < s.length(); i++) {
                char sc = s.charAt(i);
                a[sc - 'a'] += 1;
                char tc = t.charAt(i);
                b[tc - 'a'] += 1;
            }
            for (int i = 0; i < a.length; i++) {
                if(a[i] != b[i]) {
                    return false;
                }
            }
            return true;
        }
    }
}
