package com.bobby.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wenjunpei
 * in 2021/10/22 8:47.
 */
public class Test229 {
    public static void main(String[] args) {

    }
    public List<Integer> majorityElement(int[] nums) {
        Map<Integer,Integer> hashMap = new HashMap<>();
        for (int num : nums) {
            if(hashMap.containsKey(num)) {
                hashMap.put(num,hashMap.get(num) + 1);
            }else {
                hashMap.put(num,1);
            }
        }
        List<Integer> target = new ArrayList<>();
        int count = nums.length / 3;
        for (Map.Entry<Integer, Integer> entry : hashMap.entrySet()) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            if(value > count) {
                target.add(key);
            }
        }
        return target;
    }
}
