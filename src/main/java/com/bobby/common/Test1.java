package com.bobby.common;

import java.util.HashMap;

/**
 * Created by wenjunpei
 * in 2021/10/21 14:46.
 */
public class Test1 {
    public static void main(String[] args) {
        Test1 test1 = new Test1();
        int[] a = test1.twoSum(new int[]{2, 7, 11, 15}, 9);
        int[] b = test1.twoSum(new int[]{3,2,4}, 6);
        int[] c = test1.twoSum(new int[]{3,3}, 6);
    }
    public int[] twoSum(int[] nums, int target) {
        HashMap<Integer,Integer> hashMap = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if(hashMap.containsKey(target - nums[i])) {
                return new int[]{hashMap.get(target - nums[i]),i};
            }else {
                hashMap.put(nums[i],i);
            }
        }
        return null;
    }
}
