package com.bobby.common;

/**
 * Created by wenjunpei
 * in 2021/10/22 10:17.
 */
public class Test383 {
    public static void main(String[] args) {

    }
    public boolean canConstruct(String ransomNote, String magazine) {
        int[] a = new int[26];
        int[] b = new int[26];
        char[] aChar = ransomNote.toCharArray();
        char[] bChar = magazine.toCharArray();
        for (int i = 0; i < aChar.length; i++) {
            a[aChar[i] - 'a'] += 1;
        }
        for (int i = 0; i < bChar.length; i++) {
            b[bChar[i] - 'a'] += 1;
        }
        for (int i = 0; i < a.length; i++) {
            if(a[i] > b[i]) {
                return false;
            }
        }
        return true;
    }
}
