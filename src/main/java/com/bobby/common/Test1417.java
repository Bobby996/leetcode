package com.bobby.common;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wenjunpei
 * in 2021/10/22 10:30.
 */
public class Test1417 {
    public static void main(String[] args) {
        Test1417 test1417 = new Test1417();
        test1417.reformat("covid2019");
    }
    public String reformat(String s) {
        char[] array = s.toCharArray();
        List<Character> list1 = new ArrayList<>();
        List<Character> list2 = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            char c = array[i];
            if(c >= 'a' && c <= 'z') {
                list1.add(c);
            }else if(c >= '0' && c <= '9') {
                list2.add(c);
            }
        }
        if(Math.abs(list1.size() - list2.size()) > 1) {
            return "";
        }else {
            StringBuilder sb = new StringBuilder();
            if(list1.size() > list2.size()) {
                for (int i = 0; i < list2.size(); i++) {
                    sb.append(list1.get(i));
                    sb.append(list2.get(i));
                }
                sb.append(list1.get(list1.size() - 1));
            }else if(list1.size() < list2.size()){
                for (int i = 0; i < list1.size(); i++) {
                    sb.append(list2.get(i));
                    sb.append(list1.get(i));
                }
                sb.append(list2.get(list2.size() - 1));
            }else {
                for (int i = 0; i < list1.size(); i++) {
                    sb.append(list1.get(i));
                    sb.append(list2.get(i));
                }
            }
            return sb.toString();
        }
    }
}
