package com.bobby.common;

/**
 * Created by wenjunpei
 * in 2021/11/9 8:46.
 */
public class Test1513 {
    public static void main(String[] args) {
        System.out.println(numSub("0110111"));
    }
    public static int numSub(String s) {
        char[] array = s.toCharArray();
        long sum = 0;
        long count = 0 ;
        for (char c : array) {
            if(c == '1') {
                count++;
            }else {
                sum += (( 1 + count) * count ) / 2;
                count = 0;
            }
        }
        if(count != 0) {
            sum += (( 1 + count) * count ) / 2;
        }
        return (int) (sum % 1000000007);
    }
}
