package com.bobby.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by wenjunpei
 * in 2021/10/21 16:57.
 */
public class Test4 {
    public static void main(String[] args) {
        Test4 test4 = new Test4();
        double medianSortedArrays = test4.findMedianSortedArrays(new int[]{1, 2}, new int[]{3, 4});
        System.out.println(medianSortedArrays);
    }
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        List<Integer> list = new ArrayList<>();
        for (int i : nums1) {
            list.add(i);
        }
        for (int i : nums2) {
            list.add(i);
        }
        Collections.sort(list);
        if(list.size() % 2 == 0) {
            return (list.get(list.size() / 2) + list.get(list.size() / 2 - 1)) * 1.0 / 2;
        }else {
            return list.get(list.size() / 2);
        }
    }
}
